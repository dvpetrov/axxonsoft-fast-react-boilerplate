import { combineReducers } from 'redux-immutable'
import mainReducer from 'containers/Main/reducer'

export default combineReducers({
  main: mainReducer,
})
