import React, { PureComponent } from 'react'
import { hot } from 'react-hot-loader'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import { Provider } from 'react-redux'
import store from 'store'

import Main from '../Main'

class App extends PureComponent {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <Switch>
            <Route path='/' exact component={Main} />
          </Switch>
        </Router>
      </Provider>
    )
  }
}

export default hot(module)(App)
