import React, { PureComponent } from 'react'
import { func } from 'prop-types'
import Button from '@material-ui/core/Button'
import { connect } from 'react-redux'
import { actionName } from './actions'

@connect(
  () => ({}),
  { onClick: actionName }
)
export default class Main extends PureComponent {
  static propTypes = {
    onClick: func.isRequired,
  }

  render() {
    const { onClick } = this.props

    return <Button onClick={onClick}>Button (click on me)</Button>
  }
}
