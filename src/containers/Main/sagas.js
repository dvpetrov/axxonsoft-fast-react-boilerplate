import { delay } from 'redux-saga'
import { takeEvery } from 'redux-saga/effects'
import { ACTION_NAME } from './actions'

function* actionName(action) {
  yield delay(1000)

  // eslint-disable-next-line no-console
  console.log(action)
}

export default function*() {
  yield takeEvery(ACTION_NAME, actionName)
}
