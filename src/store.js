import { applyMiddleware, compose as defaultCompose, createStore } from 'redux'
import createSagaMiddleware from 'redux-saga'
import reducers from './reducers'
import sagas from './sagas'

const sagaMiddleware = createSagaMiddleware()

// eslint-disable-next-line no-underscore-dangle
const compose = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || defaultCompose

const store = createStore(reducers, compose(applyMiddleware(sagaMiddleware)))

sagaMiddleware.run(sagas)

export default store
