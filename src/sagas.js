import { all } from 'redux-saga/effects'

import main from 'containers/Main/sagas'

export default function* root() {
  yield all([main()])
}
